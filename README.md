jHelper tool to generate configuration for [Keboola Mock Server](https://github.com/keboola/ex-generic-mock-server).

Mainly usable for writer.

## Usage

- Run `uvicorn main:app --reload` to start the server
- Set the url to `localhost:8000/some-example-name/endpoint`
- Run queries against the url
- The generated mock-server configuration will be available in the `result` folder

The resulting configuration will be available at:
```
- result
  -some-example-name
     --- enpoint.request
     --- enpoint.requestHeaders
```

**NOTE** If you send multiple requests the files will be numbered e.g.:

```
- result
  -some-example-name
     --- enpoint.request
     --- enpoint.requestHeaders
     --- enpoint1.request
     --- enpoint1.requestHeaders

```
